"""
    dashboards/permissions.py
    ~~~~~~~~~~~~~~~

    Permissions Classes for dashboards app.

    :copyright: (c) 2018 by Esteban Fuentealba.
"""
import functools

from rest_framework import permissions


class AbstractUserOwnageOrSuperUserMixin(permissions.BasePermission):
    """Abatract class mixing which handles the unauth, super user or owner
    object case."""
    user_field = 'owner_id'

    def rgetattr(self, obj, attr, *args):
        """recursive getattr for nested object obj.obj2.param2."""

        def _getattr(obj, attr):
            return getattr(obj, attr, *args)

        return functools.reduce(_getattr, [obj] + attr.split('.'))

    def has_object_permission(self, request, view, obj):
        user_attribute = self.rgetattr(obj, self.user_field)
        if request.user.is_authenticated and (request.user.is_staff or
                                              request.user.is_superuser):
            return True
        if request.user.is_anonymous:
            return user_attribute is None
        return user_attribute == request.user.id


class IsBoardOwnerOrSuperUser(AbstractUserOwnageOrSuperUserMixin):
    """Object-level permission to only allow owners of a Board to edit."""
    user_field = 'owner_id'


class IsLabelOwnerOrSuperUser(AbstractUserOwnageOrSuperUserMixin):
    """Object-level permission to only allow owners of a Board to edit
    Labels."""
    user_field = 'board.owner_id'


class IsMemberOwnerOrSuperUser(AbstractUserOwnageOrSuperUserMixin):
    """Object-level permission to only allow owners of a Board to edit
    Members."""
    user_field = 'board.owner_id'


class IsBoardListOwnerOrSuperUser(AbstractUserOwnageOrSuperUserMixin):
    """Object-level permission to only allow owners of a Board to edit
    BoardLists."""
    user_field = 'board.owner_id'


class IsCardOwnerOrSuperUser(AbstractUserOwnageOrSuperUserMixin):
    """Object-level permission to only allow owners of a Board to edit
    Cards."""
    user_field = 'board_list.board.owner_id'
