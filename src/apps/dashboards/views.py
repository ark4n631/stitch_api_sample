"""
    dashboards/views.py
    ~~~~~~~~~~~~~~~

    Views for dashboards app.

    :copyright: (c) 2018 by Esteban Fuentealba.
"""
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Board, BoardList, Card, Label, Member
from .permissions import (IsBoardListOwnerOrSuperUser, IsBoardOwnerOrSuperUser,
                          IsCardOwnerOrSuperUser, IsLabelOwnerOrSuperUser,
                          IsMemberOwnerOrSuperUser)
from .serializers import (BoardListSerializer, BoardSerializer, CardSerializer,
                          LabelSerializer, MemberSerializer)


class SetOwner:
    """Abastract class to save the user as owner."""
    owner_field = 'owner'

    def perform_create(self, serializer):
        """Override perform_create to inject user for set owner."""
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
        serializer.save(**{self.owner_field: user})


class SetBoard:
    """Abastract class to save the board_id for nested actions in the
    serializer."""
    board_field = 'board_id'

    def perform_create(self, serializer):
        """Override perform_create to inject board_id for set board_id."""
        board_id = self.request.parser_context.get('kwargs', {}).get(
            'board_pk', None)
        if board_id is None:
            raise Exception('board_id could not be parsed ')
        serializer.save(**{self.board_field: board_id})


class SetBoardList:
    """Abastract class to save the board_list_id for nested actions in the
    serializer."""
    board_list_field = 'board_list_id'

    def perform_create(self, serializer):
        """Override perform_create to inject board_id for set board_id."""
        boardlist_id = self.request.parser_context.get('kwargs', {}).get(
            'boardlist_pk', None)
        if boardlist_id is None:
            raise Exception('board_id could not be parsed ')
        serializer.save(**{self.board_list_field: boardlist_id})


class FilterByOwner:
    """Abastract class to filter related objects to owner."""
    owner_field = 'owner'
    queryset = None

    def list(self, request, *args, **kwargs):  # noqa: A002,A003
        """method to filter if the user is authenticated or not to his own
        objects."""
        if self.request.user.is_authenticated:
            if not (request.user.is_superuser or request.user.is_staff):
                self.queryset = self.queryset.filter(
                    **{self.owner_field: request.user})
        else:
            self.queryset = self.queryset.filter(
                **{str(self.owner_field) + '__isnull': True})
        return super(FilterByOwner, self).list(request, *args, **kwargs)


class ArchiveMixin:
    """Abastract class to manage archive actions."""

    @action(detail=True, methods=['post'])
    def archive(  # pylint: disable=W0613
            self,
            request,
            pk=None,
            board_pk=None,
            boardlist_pk=None):
        """action to archive an instance object."""
        instance = self.get_object()
        serializer_class = self.get_serializer_class()
        instance.archived = True
        instance.save()
        context = self.get_serializer_context()
        serializer = serializer_class(instance, context=context)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(detail=True, methods=['post'])
    def unarchive(  # pylint: disable=W0613
            self,
            request,
            pk=None,
            board_pk=None,
            boardlist_pk=None):
        """action to unarchive an instance object."""
        instance = self.get_object()
        serializer_class = self.get_serializer_class()
        instance.archived = False
        instance.save()
        context = self.get_serializer_context()
        serializer = serializer_class(instance, context=context)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(detail=False)
    def archived(  # pylint: disable=W0613
            self,
            request,
            board_pk=None,
            boardlist_pk=None):
        """action to list archived objects."""
        archived = self.model.objects.filter(archived=True)
        page = self.paginate_queryset(archived)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(archived, many=True)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):  # noqa: A003
        """override list to filter out archived items."""
        self.queryset = self.queryset.exclude(archived=True)
        return super(ArchiveMixin, self).list(request, *args, **kwargs)


class BoardViewSet(ArchiveMixin, SetOwner, FilterByOwner,
                   viewsets.ModelViewSet):
    """retrieve: Return the given Board.

    list:
    Return a list of all the existing Boards.

    create:
    Create a new Board.
    """
    model = Board
    owner_field = 'owner'
    serializer_class = BoardSerializer
    queryset = Board.objects.all()
    permission_classes = [IsBoardOwnerOrSuperUser]


class LabelViewSet(SetBoard, FilterByOwner, viewsets.ModelViewSet):
    """ModelViewSet that handle Label actions."""
    serializer_class = LabelSerializer
    queryset = Label.objects.all()
    owner_field = 'board__owner'
    board_field = 'board_id'
    permission_classes = [IsLabelOwnerOrSuperUser]

    def get_queryset(self):
        return self.queryset.filter(board_id=self.kwargs['board_pk'])


class MemberViewSet(ArchiveMixin, SetBoard, FilterByOwner,
                    viewsets.ModelViewSet):
    """ModelViewSet that handle Member actions."""
    model = Member
    serializer_class = MemberSerializer
    queryset = Member.objects.all()
    owner_field = 'board__owner'
    board_field = 'board_id'
    permission_classes = [IsMemberOwnerOrSuperUser]

    def get_queryset(self):
        return self.queryset.filter(board_id=self.kwargs['board_pk'])


class BoardListViewSet(ArchiveMixin, SetBoard, FilterByOwner,
                       viewsets.ModelViewSet):
    """ModelViewSet that handle BoardList actions."""
    model = BoardList
    serializer_class = BoardListSerializer
    queryset = BoardList.objects.all()
    owner_field = 'board__owner'
    board_field = 'board_id'
    permission_classes = [IsBoardListOwnerOrSuperUser]

    def get_queryset(self):
        return self.queryset.filter(board_id=self.kwargs['board_pk'])


class CardViewSet(ArchiveMixin, SetBoardList, viewsets.ModelViewSet):
    """ModelViewSet that handle Card actions."""
    model = Card
    serializer_class = CardSerializer
    queryset = Card.objects.all().select_related(
        'board_list').prefetch_related('members', 'board_list__board')
    board_field = 'board_list__board_id'
    owner_field = 'board_list__board__owner'
    board_list_field = 'board_list_id'
    permission_classes = [IsCardOwnerOrSuperUser]

    def get_queryset(self):
        return self.queryset.filter(
            board_list__board_id=self.kwargs['board_pk'],
            board_list_id=self.kwargs['boardlist_pk'])
