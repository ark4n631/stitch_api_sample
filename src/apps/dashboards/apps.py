"""
    dashboards/apps.py
    ~~~~~~~~~~~~~

    Config for dashboards app.

    :copyright: (c) 2018 by Esteban Fuentealba.
"""
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DashboardsConfig(AppConfig):
    """Config for dashboards app."""
    name = 'dashboards'
    verbose_name = _('Dashboards')
