"""
    dashboards/serializers.py
    ~~~~~~~~~~~~~~~

    Serializers for dashboards app.

    :copyright: (c) 2018 by Esteban Fuentealba.
"""
from django.db import transaction
from rest_framework import serializers

from .models import Board, BoardList, Card, CardMember, Label, Member


class BoardSerializer(serializers.ModelSerializer):
    """Serializer for Board Model."""

    class Meta:
        model = Board
        fields = (
            'id',
            'title',
            'owner',
            'created',
            'updated',
            'archived',
        )
        read_only_fields = (
            'owner',
            'created',
            'updated',
            'archived',
        )


class MemberSerializer(serializers.ModelSerializer):
    """Serializer for Member Model."""
    board = BoardSerializer

    class Meta:
        model = Member
        fields = (
            'id',
            'first_name',
            'last_name',
            'email',
            'created',
            'updated',
            'board',
            'archived',
        )
        read_only_fields = (
            'created',
            'updated',
            'board',
            'name',
            'archived',
        )


class LabelSerializer(serializers.ModelSerializer):
    """Serializer for Label Model."""
    board = BoardSerializer

    class Meta:
        model = Label
        fields = (
            'id',
            'title',
            'created',
            'updated',
            'board',
        )
        read_only_fields = (
            'created',
            'updated',
            'board',
        )


class BoardListSerializer(serializers.ModelSerializer):
    """Serializer for ListBoard Model."""
    board = BoardSerializer

    class Meta:
        model = BoardList
        fields = (
            'id',
            'title',
            'created',
            'updated',
            'board',
            'archived',
        )
        read_only_fields = (
            'created',
            'updated',
            'board',
            'archived',
        )


class CardMemberSerializer(serializers.ModelSerializer):
    """Serializer for CardMember model m2m."""
    id = serializers.ReadOnlyField(source='member.id')  # noqa: A003
    name = serializers.ReadOnlyField(source='member.name')

    class Meta:
        model = CardMember
        fields = ('id', 'name')


class CardSerializer(serializers.ModelSerializer):
    """Serializer for Card Model."""
    board_list = BoardListSerializer
    label = LabelSerializer
    members = CardMemberSerializer(
        source='cardmember_set', many=True, required=False)

    @transaction.atomic
    def update(self, instance, validated_data):
        CardMember.objects.filter(card=instance).delete()
        members = self.initial_data.get('members', [])
        for member in members:
            member_id = member.get('id')
            try:
                new_member = Member.objects.get(pk=member_id)
                if not new_member.board_id == instance.board_list.board_id:
                    raise serializers.ValidationError(
                        'Member %d does not belong to this Board' % member_id)
            except Member.DoesNotExist:
                raise serializers.ValidationError(
                    'Member %d does not exists' % member_id)
            CardMember(card=instance, member=new_member).save()
        instance.__dict__.update(**validated_data)
        instance.save()
        return instance

    @transaction.atomic
    def create(self, validated_data):
        card = Card.objects.create(**validated_data)
        board_id = card.board_list.board_id
        if 'members' in self.initial_data:
            members = self.initial_data.get('members', [])
            for member in members:
                member_id = member.get('id')
                try:
                    member_instance = Member.objects.get(pk=member_id)
                    if not member_instance.board_id == board_id:
                        raise serializers.ValidationError(
                            'Member %d does not belong to this Board' %
                            member_id)
                except Member.DoesNotExist:
                    raise serializers.ValidationError(
                        'Member %d does not exists' % member_id)
                CardMember(card=card, member=member_instance).save()
        card.save()
        return card

    class Meta:
        model = Card
        fields = (
            'id',
            'title',
            'description',
            'created',
            'updated',
            'board_list',
            'order',
            'label',
            'members',
            'due_date',
            'members',
            'archived',
        )
        read_only_fields = (
            'created_by',
            'created',
            'updated',
            'board_list',
            'archived',
        )
