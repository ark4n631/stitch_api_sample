"""
    dashboards/models.py
    ~~~~~~~~~~~~~~~

    Models for dashboards app.

    :copyright: (c) 2018 by Esteban Fuentealba.
"""
from django.db import models
from django.utils.translation import ugettext_lazy as _
from users.models import User


class Board(models.Model):
    """Model object to describe Boards."""
    title = models.CharField(_('Title'), max_length=300)
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    updated = models.DateTimeField(_('Updated'), auto_now=True)
    archived = models.BooleanField(_('Archived'), default=False)
    owner = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name='boards',
        on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('created', )


class Label(models.Model):
    """Model object to describe Labels."""
    title = models.CharField(_('Title'), max_length=300)
    board = models.ForeignKey(
        Board, related_name='labels', on_delete=models.CASCADE)
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    updated = models.DateTimeField(_('Updated'), auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('created', )


class Member(models.Model):
    """Model object to describe Persons."""
    board = models.ForeignKey(
        Board, related_name='members', on_delete=models.CASCADE)
    first_name = models.CharField(_('First Name'), max_length=300)
    last_name = models.CharField(_('Last Name'), max_length=300)
    email = models.EmailField(blank=True, null=True, default='')
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    updated = models.DateTimeField(_('Updated'), auto_now=True)
    archived = models.BooleanField(_('Archived'), default=False)

    @property
    def name(self):
        """return user full name."""
        return str(self.first_name) + ' ' + str(self.last_name)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('created', )


class BoardList(models.Model):
    """Model object to describe BoardLists."""
    title = models.CharField(_('Title'), max_length=300)
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    updated = models.DateTimeField(_('Updated'), auto_now=True)
    board = models.ForeignKey(
        Board, related_name='lists', on_delete=models.CASCADE)
    order = models.PositiveIntegerField(_('order'), default=1)
    archived = models.BooleanField(_('Archived'), default=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order', )


class Card(models.Model):
    """Model object to describe Cards."""
    title = models.CharField(_('Title'), max_length=300)
    description = models.TextField(_('Description'), blank=True)
    due_date = models.DateField(_('Due Date'), blank=True, null=True)
    label = models.ForeignKey(
        Label, blank=True, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    updated = models.DateTimeField(_('Updated'), auto_now=True)
    board_list = models.ForeignKey(
        BoardList, related_name='cards', on_delete=models.CASCADE)
    order = models.PositiveIntegerField(_('order'), default=1)
    members = models.ManyToManyField(
        Member, related_name='cards', through='CardMember')
    archived = models.BooleanField(_('Archived'), default=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order', )


class CardMember(models.Model):
    """Model object to describe Card and Member through relation."""
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    card = models.ForeignKey(Card, on_delete=models.CASCADE)

    def __str__(self):
        return self.member.name + ' ' + self.card.title
