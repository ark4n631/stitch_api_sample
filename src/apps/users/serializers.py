"""
    users/serializers.py
    ~~~~~~~~~~~~~~~

    Serializers for users app.

    :copyright: (c) 2018 by Esteban Fuentealba.
"""
from rest_framework import serializers

from .models import User


class UserSerializer(serializers.ModelSerializer):
    """Serializer for User Model."""

    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
            'email',
            'last_login',
            'is_superuser',
            'is_staff',
            'date_joined',
            'full_name',
            'password',
        )
        read_only_fields = (
            'last_login',
            'is_superuser',
            'is_staff',
            'date_joined',
            'full_name',
        )
        write_only_fields = ('password', )
