from django.test import TestCase
from tests.utils import GenericTestModelSerializer
from dashboards.models import Member
from dashboards.serializers import MemberSerializer


class TestMemberSerializer(TestCase, GenericTestModelSerializer):
    serializer = MemberSerializer
    model = Member

    def setUp(self):
        self.set_initial_data()
