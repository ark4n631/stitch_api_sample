from django.test import TestCase
from tests.utils import GenericTestModelSerializer
from dashboards.models import Label
from dashboards.serializers import LabelSerializer


class TestLabelSerializer(TestCase, GenericTestModelSerializer):
    serializer = LabelSerializer
    model = Label

    def setUp(self):
        self.set_initial_data()
