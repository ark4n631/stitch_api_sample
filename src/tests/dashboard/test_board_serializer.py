from django.test import TestCase
from tests.utils import GenericTestModelSerializer
from dashboards.models import Board
from dashboards.serializers import BoardSerializer


class TestBoardSerializer(TestCase, GenericTestModelSerializer):
    serializer = BoardSerializer
    model = Board

    def setUp(self):
        self.set_initial_data()
