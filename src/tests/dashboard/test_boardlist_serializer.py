from django.test import TestCase
from tests.utils import GenericTestModelSerializer
from dashboards.models import BoardList
from dashboards.serializers import BoardListSerializer


class TestBoardListSerializer(TestCase, GenericTestModelSerializer):
    serializer = BoardListSerializer
    model = BoardList

    def setUp(self):
        self.set_initial_data()
