from copy import copy
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from tests.utils import CustomTestClient, GenericViewSetTestClass, BoardGenMixin, ArchiveMixin
from dashboards.models import Board


class TestBoardEndPoint(APITestCase, CustomTestClient, GenericViewSetTestClass, BoardGenMixin, ArchiveMixin):
    model = Board

    def _update_url(self):
        pass

    def setUp(self):
        super(TestBoardEndPoint, self).setUp()
        self.mock_data = {'title': 'My board'}
        self.edit_data = {'title': 'edited Board'}
        self.url = reverse('board-list')

    def _delete_test(self, user=None):
        """based on a user generate a object and assert that can be deleted"""
        obj = self.generate_models(1, self.model, owner=user)[0]
        model_id = copy(obj.id)
        response = self.client.delete(self.url + '{id}/'.format(id=obj.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get(self.url + '{id}/'.format(id=model_id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def _edit_test(self, user=None):
        """based on a user generate a object and assert that can be edited"""
        obj = self.generate_models(1, self.model, owner=user)[0]
        response = self.client.patch(self.url + '{id}/'.format(id=obj.id),
                                     data=self.edit_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('id'), obj.id)
        for att in self.edit_data:
            self.assertEqual(self.edit_data.get(att), response.json().get(att))

    def _retrieve_test(self, user=None):
        """based on a user generate a object and assert that can be fetched"""
        obj = self.generate_models(1, self.model, owner=user)[0]
        response = self.client.get(self.url + '{id}/'.format(id=obj.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('id'), obj.id)

    def _list_test(self, user=None):
        """based on a user generate 3 objects and assert they are fetched"""
        mock_models = self.generate_models(3, self.model, owner=user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('results')), 3)
        for result in response.json().get('results'):
            self.assertTrue(result.get('id') in [obj.id for obj in mock_models])

    def _archive(self, user=None):
        obj = self.generate_models(1, self.model, owner=user)[0]
        response = self.client.post(self.url + '{id}/archive/'.format(id=obj.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('id'), obj.id)
        self.assertTrue(response.json().get('archived'))

    def _unarchive(self, user=None):
        obj = self.generate_models(1, self.model, owner=user, archived=True)[0]
        response = self.client.post(self.url + '{id}/unarchive/'.format(id=obj.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('id'), obj.id)
        self.assertFalse(response.json().get('archived'))

    def _archived(self, user=None):
        mock_models = self.generate_models(3, self.model, owner=user, archived=True)
        response = self.client.get(self.url + 'archived/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('results')), 3)
        for result in response.json().get('results'):
            self.assertTrue(result.get('id') in [obj.id for obj in mock_models])
