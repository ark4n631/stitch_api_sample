from copy import copy
from django_dynamic_fixture import G
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from tests.utils import CustomTestClient, GenericViewSetTestClass, BoardGenMixin
from dashboards.models import Board, Label

class TestLabelEndPoint(APITestCase, CustomTestClient, GenericViewSetTestClass, BoardGenMixin):
    model = Label

    def _update_url(self):
        self.url = reverse('board-labels-list', kwargs={'board_pk': self.board.id})

    def setUp(self):
        super(TestLabelEndPoint, self).setUp()
        self.board = G(Board, owner=None)
        self.mock_data = {'title': 'My Label'}
        self.edit_data = {'title': 'edited Label'}
        self.url = reverse('board-labels-list', kwargs={'board_pk': self.board.id})

    def _retrieve_test(self, user=None):
        """based on a user generate a object and assert that can be fetched"""
        self.board.owner = user
        self.board.save()
        obj = self.generate_models(1, self.model, board=self.board)[0]
        response = self.client.get(self.url + '{id}/'.format(id=obj.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('id'), obj.id)

    def _list_test(self, user=None):
        self.board.owner = user
        self.board.save()
        mock_models = self.generate_models(3, self.model, board=self.board)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('results')), 3)
        for result in response.json().get('results'):
            self.assertTrue(result.get('id') in [obj.id for obj in mock_models])

    def _edit_test(self, user=None):
        """based on a user generate a object and assert that can be edited"""
        self.board.owner = user
        self.board.save()
        obj = self.generate_models(1, self.model, board=self.board)[0]
        response = self.client.patch(self.url + '{id}/'.format(id=obj.id),
                                     data=self.edit_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('id'), obj.id)
        for att in self.edit_data:
            self.assertEqual(self.edit_data.get(att), response.json().get(att))

    def _delete_test(self, user=None):
        """based on a user generate a object and assert that can be deleted"""
        self.board.owner = user
        self.board.save()
        obj = self.generate_models(1, self.model, board=self.board)[0]
        model_id = copy(obj.id)
        response = self.client.delete(self.url + '{id}/'.format(id=obj.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get(self.url + '{id}/'.format(id=model_id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


