from django.test import TestCase
from tests.utils import GenericTestModelSerializer
from dashboards.models import Card, CardMember
from dashboards.serializers import CardSerializer, CardMemberSerializer


class TestCardSerializer(TestCase, GenericTestModelSerializer):
    serializer = CardSerializer
    model = Card

    def setUp(self):
        self.set_initial_data()


class TestCardMemberSerializer(TestCase, GenericTestModelSerializer):
    serializer = CardMemberSerializer
    model = CardMember

    def test_serializer_with_empty_data(self):
        """Override this method because is a read only model so empty is valid"""
        serializer = self.serializer(data={})
        self.assertTrue(serializer.is_valid())

    def setUp(self):
        self.set_initial_data()
