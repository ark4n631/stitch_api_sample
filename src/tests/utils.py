"""
Test utils
"""
from copy import copy

import pytest
from django_dynamic_fixture import G, N, F
from rest_framework import status
from rest_framework.test import APIClient
from users.models import User
from dashboards.models import Board, BoardList

class CustomTestClient(object):

    def setUp(self):
        """setUp the APIClient"""
        self.client = APIClient()
        return super(CustomTestClient, self).setUp()

    def login(self, user, password=None, superuser=False, delete_user=True):
        password = password or 'password'
        try:
            user_obj = User.objects.filter(username=user).first()
        except Exception:
            user_obj = None
        if user_obj and delete_user:
            user_obj.delete()
            user_obj = None
        if not user_obj and superuser:
            user_obj = G(User, username=user, is_superuser=True)
        elif user_obj and superuser:
            user_obj.is_superuser = True
        elif not user_obj:
            user_obj = G(User, username=user)

        user_obj.set_password(password)
        user_obj.save()
        data = {'username': user_obj.username, 'password': password}
        token = self.client.post('/api/token/', data=data).json()
        header = ' '.join(['Bearer', token.get('access')])
        self.client.credentials(HTTP_AUTHORIZATION=header)
        return user_obj

    def logout(self, user=None):
        if user:
            user.delete()
        self.client.credentials()


class GenericTestModelSerializer:
    model = None
    serializer = None
    mock_data = {}

    def set_initial_data(self, *args, **kwargs):
        _disposable = N(self.model).__dict__
        self.mock_data = _disposable

    def test_serializer_with_empty_data(self):
        serializer = self.serializer(data={})
        self.assertFalse(serializer.is_valid())

    @pytest.mark.django_db
    def test_serializer_with_valid_data(self):
        serializer = self.serializer(data=self.mock_data)
        self.assertTrue(serializer.is_valid())


class GenericViewSetTestClass:

    def generate_models(self, amount=1, model=None, **kwargs):
        objects = []
        for number in range(0, amount):
            model_store = G(self.model if model is None else model)
            for attr, value in kwargs.items():
                setattr(model_store, attr, value)
            model_store.save()
            objects.append(model_store)
        return objects


    def test_empty_create_request_fails(self):
        """form validates"""
        response = self.client.post(self.url, {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @pytest.mark.django_db
    def test_anonymous_user_create(self):
        """Non anonymous user is allowed to create"""
        response = self.client.post(self.url, data=self.mock_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    @pytest.mark.django_db
    def test_superuser_create(self):
        """Test that superuser can create"""
        self.login('root', superuser=True)
        response = self.client.post(self.url, data=self.mock_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    @pytest.mark.django_db
    def test_normal_user_create(self):
        """Test that user can create"""
        self.login('someone')
        response = self.client.post(self.url, data=self.mock_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    @pytest.mark.django_db
    def test_anonymous_list(self):
        self._list_test()

    @pytest.mark.django_db
    def test_user_list(self):
        user = self.login('normaluser')
        self._list_test(user=user)

    @pytest.mark.django_db
    def test_superuser_list(self):
        user = self.login('root', superuser=True)
        self._list_test(user=user)

    @pytest.mark.django_db
    def test_anonymous_retrieve(self):
        """test that non auth, user and admin users may retrieve detail"""
        self._retrieve_test()

    @pytest.mark.django_db
    def test_user_retrieve(self):
        user = self.login('normaluser')
        self._retrieve_test(user=user)

    @pytest.mark.django_db
    def test_superuser_retrieve(self):
        user = self.login('root', superuser=True)
        self._retrieve_test(user=user)

    @pytest.mark.django_db
    def test_anonymous_delete(self):
        self._delete_test()

    @pytest.mark.django_db
    def test_user_delete(self):
        user = self.login('normaluser')
        self._delete_test(user=user)

    @pytest.mark.django_db
    def test_superuser_delete(self):
        user = self.login('root', superuser=True)
        self._delete_test(user=user)

    @pytest.mark.django_db
    def test_anonymous_edit(self):
        """test that anonymous can edit"""
        self._edit_test()

    @pytest.mark.django_db
    def test_user_edit(self):
        """test that logged user can edit"""
        user = self.login('normaluser')
        self._edit_test(user=user)

    @pytest.mark.django_db
    def test_superuser_edit(self):
        """test that superuser can edit"""
        user = self.login('root', superuser=True)
        self._edit_test(user=user)

    @pytest.mark.django_db
    def test_anonymous_cannot_edit_others(self):
        board = self._gen_non_owner_board()
        if hasattr(self, 'board_list'):
            board_list = self._get_non_owner_boardlist(board=board)
            setattr(self, 'board', board)
            setattr(self, 'board_list', board_list)
            obj = self.generate_models(1, self.model, board_list=self.board_list)[0]
        elif hasattr(self, 'board'):
            setattr(self, 'board', board)
            obj = self.generate_models(1, self.model, board=self.board)[0]
        else:
            obj = board
        self._update_url()
        response = self.client.patch(self.url + '{id}/'.format(id=obj.id),
                                     data=self.edit_data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    @pytest.mark.django_db
    def test_normal_user_cannot_edit_others(self):
        self.login('normaluser')
        board = self._gen_non_owner_board()
        if hasattr(self, 'board_list'):
            board_list = self._get_non_owner_boardlist(board=board)
            setattr(self, 'board', board)
            setattr(self, 'board_list', board_list)
            obj = self.generate_models(1, self.model, board_list=self.board_list)[0]
        elif hasattr(self, 'board'):
            setattr(self, 'board', board)
            obj = self.generate_models(1, self.model, board=self.board)[0]
        else:
            obj = board
        self._update_url()
        response = self.client.patch(self.url + '{id}/'.format(id=obj.id),
                                     data=self.edit_data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @pytest.mark.django_db
    def test_anonymous_cannot_delete_others(self):
        board = self._gen_non_owner_board()
        if hasattr(self, 'board_list'):
            board_list = self._get_non_owner_boardlist(board=board)
            setattr(self, 'board', board)
            setattr(self, 'board_list', board_list)
            obj = self.generate_models(1, self.model, board_list=self.board_list)[0]
        elif hasattr(self, 'board'):
            setattr(self, 'board', board)
            obj = self.generate_models(1, self.model, board=self.board)[0]
        else:
            obj = board
        self._update_url()
        response = self.client.delete(self.url + '{id}/'.format(id=obj.id))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    @pytest.mark.django_db
    def test_normal_user_cannot_delete_others(self):
        self.login('normaluser')
        board = self._gen_non_owner_board()
        if hasattr(self, 'board_list'):
            board_list = self._get_non_owner_boardlist(board=board)
            setattr(self, 'board', board)
            setattr(self, 'board_list', board_list)
            obj = self.generate_models(1, self.model, board_list=self.board_list)[0]
        elif hasattr(self, 'board'):
            setattr(self, 'board', board)
            obj = self.generate_models(1, self.model, board=self.board)[0]
        else:
            obj = board
        self._update_url()
        response = self.client.delete(self.url + '{id}/'.format(id=obj.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @pytest.mark.django_db
    def test_superuser_can_delete_anyone(self):
        self.login('root', superuser=True)
        board = self._gen_non_owner_board()
        if hasattr(self, 'board_list'):
            board_list = self._get_non_owner_boardlist(board=board)
            setattr(self, 'board', board)
            setattr(self, 'board_list', board_list)
            obj = self.generate_models(1, self.model, board_list=self.board_list)[0]
        elif hasattr(self, 'board'):
            setattr(self, 'board', board)
            obj = self.generate_models(1, self.model, board=self.board)[0]
        else:
            obj = board
        self._update_url()
        model_id = copy(obj.id)
        response = self.client.delete(self.url + '{id}/'.format(id=obj.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get(self.url + '{id}/'.format(id=model_id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @pytest.mark.django_db
    def test_superuser_can_edit_anyone(self):
        self.login('root', superuser=True)
        board = self._gen_non_owner_board()
        if hasattr(self, 'board_list'):
            board_list = self._get_non_owner_boardlist(board=board)
            setattr(self, 'board', board)
            setattr(self, 'board_list', board_list)
            obj = self.generate_models(1, self.model, board_list=self.board_list)[0]
        elif hasattr(self, 'board'):
            setattr(self, 'board', board)
            obj = self.generate_models(1, self.model, board=self.board)[0]
        else:
            obj = board
        self._update_url()
        response = self.client.patch(self.url + '{id}/'.format(id=obj.id),
                                     data=self.edit_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('id'), obj.id)
        for att in self.edit_data:
            self.assertEqual(self.edit_data.get(att), response.json().get(att))


class BoardGenMixin:

    def _gen_non_owner_board(self):
        """Generate a object that it doesnt belong to logged user or anonymous"""
        user1 = G(User)
        obj = self.generate_models(1, Board, owner=user1)[0]
        return obj

    def _get_non_owner_boardlist(self, board):
        obj = self.generate_models(1, BoardList, board=board)[0]
        return obj


class ArchiveMixin:

    @pytest.mark.django_db
    def test_anonymous_archive(self):
        self._archive()

    @pytest.mark.django_db
    def test_user_archive(self):
        user = self.login('normaluser')
        self._archive(user=user)

    @pytest.mark.django_db
    def test_superuser_archive(self):
        user = self.login('root', superuser=True)
        self._archive(user=user)

    @pytest.mark.django_db
    def test_anonymous_unarchive(self):
        self._unarchive()

    @pytest.mark.django_db
    def test_user_unarchive(self):
        user = self.login('normaluser')
        self._unarchive(user=user)

    @pytest.mark.django_db
    def test_superuser_unarchive(self):
        user = self.login('root', superuser=True)
        self._unarchive(user=user)

    @pytest.mark.django_db
    def test_anonymous_archived(self):
        self._archived()

    @pytest.mark.django_db
    def test_user_archived(self):
        user = self.login('normaluser')
        self._archived(user=user)

    @pytest.mark.django_db
    def test_superuser_archived(self):
        user = self.login('root', superuser=True)
        self._archived(user=user)
