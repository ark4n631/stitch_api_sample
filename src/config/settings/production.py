"""
Production Configurations

- Force SECRET_KEY to exists

"""
from __future__ import absolute_import, unicode_literals
from .base import *  # noqa

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Raises ImproperlyConfigured exception if DJANGO_SECRET_KEY not in os.environ
SECRET_KEY = env('DJANGO_SECRET_KEY')

INSTALLED_APPS += ['gunicorn', ]
