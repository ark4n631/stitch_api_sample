"""
    config/routers.py
    ~~~~~~~~~~~~~~~~~

    All api routers for apps.

    :copyright: (c) 2018 by Esteban Fuentealba.
"""
from rest_framework_nested import routers
from dashboards.views import BoardViewSet, LabelViewSet, MemberViewSet, BoardListViewSet, CardViewSet

router_v1 = routers.DefaultRouter()

router_v1.register(r'boards', BoardViewSet)

boards_router = routers.NestedSimpleRouter(router_v1, r'boards', lookup='board')
boards_router.register(r'labels', LabelViewSet, basename='board-labels')
boards_router.register(r'members', MemberViewSet, basename='board-members')
boards_router.register(r'boardlists', BoardListViewSet, basename='board-boardlists')

cards_router = routers.NestedSimpleRouter(boards_router, r'boardlists', lookup='boardlist')
cards_router.register(r'cards', CardViewSet, basename='cards')
