# Stitch API Sample

Code sample for stitch API Rest

API to mimic trello board actions

You can find the docs in [api/docs](http://104.248.54.93/api/docs/)
and the endpoints in [api/v1/](http://104.248.54.93/api/v1/)
You will find good examples of the request in the docs
The ip is `104.248.54.93`

The DB diagram can be found here:
[db_graph.png](https://gitlab.com/ark4n631/stitch_api_sample/blob/master/db_graph.png)

Caveats: 

Any user can create Board, and related objects, 
but if a user is registered it will show only his own boards and related objects,
this means that any anonymous (or superuser) can change the non auth data, but if they
create the data with their account only them can see and edit it.

Superusers have complete access to any board or edit any data.

Test cases are abstract beause i tried to no repeat code as much as possible
you can find the abstract test classes in 

[src/tests/utils.py](https://gitlab.com/ark4n631/stitch_api_sample/blob/master/src/tests/utils.py)

Also there are many abstract classes and helpers that use inheritance that you may want to take a look:

[src/apps/dashboards/permissions.py](https://gitlab.com/ark4n631/stitch_api_sample/blob/master/src/apps/dashboards/permissions.py)

[src/apps/dashboards/views.py#L19-60](https://gitlab.com/ark4n631/stitch_api_sample/blob/master/src/apps/dashboards/views.py#L19-60)

API created with cookiecutter, boilterplate taken from

https://github.com/abogoyavlensky/cookiecutter-django-api

----

## Requirements

* Python 3.6.4
* Django 2.x
* Django rest framework 3.8

## Local development using Docker

Before you start please install docker and docker-compose on the host.
Then you could perfom several command using `make`:

```bash
$ make build
$ make detach [service]  # now you could open http://localhost:8000/
$ make logs [service]
$ make stop
```

To run tests, check linting you have next command:
```bash
$ make test api  # Run test and recreate db every time
$ make watch api # Run tests in watching mode using already created db
$ make fmt api # Autoformatting and linting source code
```

All available commands you could check at running:

```bash
$ make help
```

## Install git-hook on pre-commit

To install git-hook you should install python package `pre-commit`
(https://pre-commit.com/) and run it locally:

```bash
$ pip install pre-commit
$ pre-commit install
```
